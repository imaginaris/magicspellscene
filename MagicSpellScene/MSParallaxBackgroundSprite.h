//
//  MSParallaxScene.h
//  MagicSpellScene
//
//  Created by Developer on 7/3/14.
//  Copyright (c) 2014 DaftCode. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MSParallaxBackgroundSprite : SKSpriteNode

@property (nonatomic) BOOL usesParallaxEffect;
@property (nonatomic) CGFloat virtualZRotation;
@property (nonatomic) CGFloat parallaxOffset;

- (id)initWithSprites:(NSArray *)sprites usingOffset:(CGFloat)offset;
- (void)addSprite:(SKSpriteNode *)sprite;
- (void)removeSprite:(SKSpriteNode *)sprite;
- (void)removeAllSprites;

- (void)updateOffset;

@end
