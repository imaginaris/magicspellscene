//
//  MSTerrainGenerator.m
//  MagicSpellScene
//
//  Created by Developer on 7/4/14.
//  Copyright (c) 2014 DaftCode. All rights reserved.
//

#import "MSTerrainGenerator.h"

@implementation MSTerrainGenerator

+ (UIBezierPath *)generateTerrainWithSize:(CGSize)size granulatiry:(NSUInteger)gran
{
    return [self generateTerrainWithSize:size distortionLevel:1.0 granularity:gran smoothed:NO];
}

+ (UIBezierPath *)generateTerrainWithSize:(CGSize)size distortionLevel:(CGFloat)level granularity:(NSUInteger)gran
{
    return [self generateTerrainWithSize:size distortionLevel:level granularity:gran smoothed:NO];
}

+ (UIBezierPath *)generateTerrainWithSize:(CGSize)size distortionLevel:(CGFloat)level granularity:(NSUInteger)gran smoothed:(BOOL)smooth
{
    NSMutableArray *points = [[NSMutableArray alloc] init];
    CGFloat absoluteRange = 1.0;
    CGFloat heightCenter = size.height/2.0;
    NSUInteger regions = 1;
    CGFloat modifier = powf(2, -level);
    CGFloat subregionStartX = size.width/(regions*2);
    
    [points addObjectsFromArray:@[[NSValue valueWithCGPoint:CGPointMake(0, heightCenter)],
                                  [NSValue valueWithCGPoint:CGPointMake(size.width, heightCenter)]]];
    for (int i=0; i<gran; i++) {
        srand48(time(0));
        for (int j=0; j<regions*2; j+=2) {
            CGFloat threshold = drand48() * absoluteRange*2 - absoluteRange;
            CGFloat regionLeftY = [self heightForX:subregionStartX*j inArray:points];
            CGFloat regionRightY = [self heightForX:subregionStartX*(j+2) inArray:points];
            CGFloat heightDiff = regionRightY-regionLeftY;
            CGFloat calculatedY = regionLeftY+heightDiff/2+threshold*heightCenter;
            CGPoint point = CGPointMake(subregionStartX*(j+1), calculatedY);
            [points addObject:[NSValue valueWithCGPoint:point]];
        }
        regions *= 2;
        subregionStartX /= 2;
        absoluteRange *= modifier;
    }
    
    points = [self sortPointsArray:points];
    return [self createBezierPathForPoints:points smoothed:smooth];
}

+ (CGFloat)heightForX:(CGFloat)x inArray:(NSArray *)pArray
{
    __block CGFloat y = 0.0;
    [pArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CGPoint point = [obj CGPointValue];
        if (point.x == x) {
            y = point.y;
            *stop = YES;
        }
    }];
    return y;
}

+ (NSMutableArray *)sortPointsArray:(NSMutableArray *)pArray
{
    [pArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        CGPoint point1 = [obj1 CGPointValue];
        CGPoint point2 = [obj2 CGPointValue];
        
        if (point1.x > point2.x) return NSOrderedDescending;
        else if (point1.x < point2.x) return NSOrderedAscending;
        else return NSOrderedSame;
    }];
    return pArray;
}

+ (UIBezierPath *)createBezierPathForPoints:(NSArray *)points smoothed:(BOOL)smoothed
{
    if (!points || !points.count) {
        return nil;
    }
    
    if (!smoothed) {
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint:[points[0] CGPointValue]];
        [points enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if (idx) {
                CGPoint point = [obj CGPointValue];
                [bezierPath addLineToPoint:point];
            }
        }];
        return bezierPath;
    }
    else {
        //use quad curve
        return nil;
    }
}

@end
