//
//  MSSceneModel.h
//  MagicSpellScene
//
//  Created by Developer on 7/4/14.
//  Copyright (c) 2014 DaftCode. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef NS_ENUM(NSUInteger, MSSceneModelWorldLayer) {
    MSSceneModelWorldLayerBackground = 0,
    MSSceneModelWorldLayerGround,
    MSSceneModelWorldLayerCharacter,
    MSSceneModelWorldLayerAboveCharacter,
    MSSceneModelWorldLayerTop,
    MSSceneModelWorldLayerCount
};

@interface MSSceneModel : SKScene <SKPhysicsContactDelegate>

@end
