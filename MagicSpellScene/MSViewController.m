//
//  MSViewController.m
//  MagicSpellScene
//
//  Created by Developer on 7/3/14.
//  Copyright (c) 2014 DaftCode. All rights reserved.
//

#import "MSViewController.h"
#import "MSMyScene.h"

@interface MSViewController ()

@property (nonatomic) MSMyScene *scene;

@end

@implementation MSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    skView.ignoresSiblingOrder = YES;
    
    [MSMyScene loadSceneAssetsWithCompletionHandler:^{
        CGSize viewSize = skView.bounds.size;
        
        // On iPhone/iPod touch we want to see a similar amount of the scene as on iPad.
        // So, we set the size of the scene to be double the size of the view, which is
        // the whole screen, 3.5- or 4- inch. This effectively scales the scene to 50%.
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            viewSize.height *= 2;
            viewSize.width *= 2;
        }
        
        MSMyScene *scene = [[MSMyScene alloc] initWithSize:viewSize];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        self.scene = scene;
        
        [skView presentScene:scene];
    }];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

//- (NSUInteger)supportedInterfaceOrientations
//{
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//        return UIInterfaceOrientationMaskAllButUpsideDown;
//    } else {
//        return UIInterfaceOrientationMaskAll;
//    }
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
