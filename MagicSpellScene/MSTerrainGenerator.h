//
//  MSTerrainGenerator.h
//  MagicSpellScene
//
//  Created by Developer on 7/4/14.
//  Copyright (c) 2014 DaftCode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MSTerrainGenerator : NSObject

//level (0.0, 1.0), granularity (1<) 2^gran podzialow
+ (UIBezierPath *)generateTerrainWithSize:(CGSize)size granulatiry:(NSUInteger)gran;
+ (UIBezierPath *)generateTerrainWithSize:(CGSize)size distortionLevel:(CGFloat)level granularity:(NSUInteger)gran;
+ (UIBezierPath *)generateTerrainWithSize:(CGSize)size distortionLevel:(CGFloat)level granularity:(NSUInteger)gran smoothed:(BOOL)smooth;

@end
