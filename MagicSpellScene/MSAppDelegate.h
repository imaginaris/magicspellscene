//
//  MSAppDelegate.h
//  MagicSpellScene
//
//  Created by Developer on 7/3/14.
//  Copyright (c) 2014 DaftCode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
