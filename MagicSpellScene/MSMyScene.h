//
//  MSMyScene.h
//  MagicSpellScene
//

//  Copyright (c) 2014 DaftCode. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "MSSceneModel.h"

@interface MSMyScene : MSSceneModel

+ (void)loadSceneAssetsWithCompletionHandler:(void (^)())handler;

@end
