//
//  MSMyScene.m
//  MagicSpellScene
//
//  Created by Developer on 7/3/14.
//  Copyright (c) 2014 DaftCode. All rights reserved.
//

#import "MSMyScene.h"
#import "MSParallaxBackgroundSprite.h"
#import "MSTerrainGenerator.h"

@interface MSMyScene () {
    BOOL moveEnabled;
    BOOL moveRight; //lame...
}

@property (nonatomic, strong) SKNode *world;

@end

@implementation MSMyScene

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];
        
        _world = [[SKNode alloc] init];
        [_world setName:@"world"];
//        _layers = [NSMutableArray arrayWithCapacity:kWorldLayerCount];
//        for (int i = 0; i < kWorldLayerCount; i++) {
//            SKNode *layer = [[SKNode alloc] init];
//            layer.zPosition = i - kWorldLayerCount;
//            [_world addChild:layer];
//            [(NSMutableArray *)_layers addObject:layer];
//        }
        
        [self addChild:_world];
        
        [self buildWorld];
        
        [self.world setPosition:CGPointMake(-(0) + CGRectGetMidX(self.frame),
                                            -(0) + CGRectGetMidY(self.frame))];
    }
    return self;
}

- (void)buildWorld {
    NSLog(@"Building the world");
    
    // Configure physics for the world.
    self.physicsWorld.gravity = CGVectorMake(1.0f, 9.8f);
    self.physicsWorld.contactDelegate = self;
    
    [self addBackground];
}

- (void)addBackground {
    UIBezierPath *terrainPath = [MSTerrainGenerator generateTerrainWithSize:CGSizeMake(1000, 400) distortionLevel:1.0 granularity:7];
    
    for (SKNode *bgNode in [self bgNodes]) {
        [self.world addChild:bgNode];
        //[self addNode:tileNode atWorldLayer:APAWorldLayerGround];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
//    for (UITouch *touch in touches) {
//        CGPoint location = [touch locationInNode:self];
//        
//        SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:@"Spaceship"];
//        
//        sprite.position = location;
//        
//        SKAction *action = [SKAction rotateByAngle:M_PI duration:1];
//        
//        [sprite runAction:[SKAction repeatActionForever:action]];
//        
//        [self addChild:sprite];
//    }

    moveEnabled = YES;
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    moveRight = (touchLocation.x >= self.view.bounds.size.width/2);
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    moveEnabled = NO;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    moveEnabled = NO;
}

- (void)updateTouchOffset:(NSTimer *)timer
{

}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

- (void)didSimulatePhysics
{
    if (moveEnabled) {
        CGPoint worldPos = self.world.position;
        if (moveRight) {
            worldPos.x -= 1;
        }
        else {
            worldPos.x += 1;
        }
        self.world.position = worldPos;
        
        for (MSParallaxBackgroundSprite *bgSprite in [self bgNodes]) {
            [bgSprite updateOffset];
        }
    }
}

+ (void)loadSceneAssetsWithCompletionHandler:(void (^)())handler {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        // Load the shared assets in the background.
        [self loadSceneAssets];
        
        if (!handler) {
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Call the completion handler back on the main queue.
            handler();
        });
    });
}

+ (void)loadSceneAssets {
    MSParallaxBackgroundSprite *bgSprite = [[MSParallaxBackgroundSprite alloc] initWithSprites:@[
                                                    [SKSpriteNode spriteNodeWithImageNamed:@"bg1"],
                                                    [SKSpriteNode spriteNodeWithImageNamed:@"bg2"]
                                                                                                 ] usingOffset:1500.0f];
    bgSprite.zPosition = -1;
    
    sBgNodes = [[NSMutableArray alloc] initWithCapacity:5];
    CGFloat xOffset = [UIImage imageNamed:@"bg1"].size.width;
    for (int i=0; i<5; i++) {
        MSParallaxBackgroundSprite *bg = [bgSprite copy];
        bg.position = CGPointMake(i*xOffset, 0);
        //tileNode.blendMode = SKBlendModeReplace;
        [sBgNodes addObject:bg];
    }
}

static NSMutableArray *sBgNodes = nil;
- (NSArray *)bgNodes {
    return sBgNodes;
}

@end
